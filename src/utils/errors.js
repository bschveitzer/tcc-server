exports.loginErrors = {
  NotFound: {
    message: 'Credenciais inválidas.'
  }
}

exports.recoverPasswordErrors = {
  NotFound: {
    message: 'E-mail não cadastrado.'
  },
  SendingError: {
    message: 'Erro ao recuperar senha, tente novamente.'
  },
  InvalidId: {
    message: 'Usuário inválido.'
  },
  ExpiredSolicitation: {
    message: 'Solicitação expirada, refaça a solicitação.'
  }
}

exports.crudUserErrors = {
  ValidationError: {
    message: 'Faltam informações do usuário.'
  },
  MongoError: {
    message: 'Usuário já cadastrado no sistema.',
    action: 'recoverPassword'
  }
}