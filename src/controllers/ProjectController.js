const mongoose = require('mongoose')
const Project = mongoose.model('Project')
const User = mongoose.model('User')
const Sprint = mongoose.model('Sprint')
const error = require('../utils/errors')

module.exports = {
  getProjectDetails: async (req, res) => {
    const project = await Project.findById(req.params.projectId).populate('users').populate('tasks').populate('sprints')
    return res.json(project)
  },
  getProjectsFromOrg: async (req, res) => {
    const { page = 1 } = req.query
    const projects = await Project.paginate({ organization: req.params.organizationId }, { page, limit: 10 })
    return res.json(projects)
  },
  createProject: async (req, res) => {
    try {
      const project = await Project.create(req.body)
      return res.json(project)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  updateProject: async (req, res) => {
    try {
      const project = await Project.findByIdAndUpdate(req.params.projectId, req.body, { new: true }).populate('users').populate('tasks').populate('sprints')
      return res.json(project)
    } catch (err) {    
      return res.status(400).json(err)
    }
  },
  addUserProject: async (req, res) => {
    try {
      const project = await Project.findByIdAndUpdate(req.params.projectId, { $addToSet: { 'users': req.body.userId } }, { new: true }).populate('users').populate('tasks').populate('sprints')
      return res.json(project)
    } catch (err) {    
      return res.status(400).json(err)
    }
  },
  removeUserProject: async (req, res) => {
    try {
      const project = await Project.findByIdAndUpdate(req.params.projectId, { $pull: { 'users': req.body.userId } }, { new: true }).populate('users').populate('tasks').populate('sprints')
      return res.json(project)
    } catch (err) {    
      return res.status(400).json(err)
    }
  },
  createNewSprint: async (req, res) => {
    try {
      const maxSprint = await Sprint.find({project: req.params.projectId}).sort({number:-1}).limit(1);
      const number = maxSprint.length ? maxSprint[0].number + 1 : 1;
      const newSprintObj = {
        name: 'Sprint ' + number,
        number,
        project: req.params.projectId
      }

      const newSprint = await Sprint.create(newSprintObj);

      const projectUpdated = await Project.findByIdAndUpdate(req.params.projectId, { $addToSet: { 'sprints': newSprint._id } }, { new: true })
      .populate('users')
      .populate('sprints')
      .populate('tasks')

      return res.json(projectUpdated)
    } catch (err) {    
      return res.status(400).json(err)
    }
  },
  filterTasksBySprint: async (req, res) => {
    try {
      const filteredTasks = await Sprint.findOne({_id: req.params.sprintId, project: req.params.projectId}).populate('tasks')
      return res.json(filteredTasks)
    } catch(err) {
      return res.status(400).json(err)
    }
  }
}