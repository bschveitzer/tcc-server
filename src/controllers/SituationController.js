const mongoose = require('mongoose')
const Situation = mongoose.model('Situation')
const error = require('../utils/errors')

module.exports = {
  getAllSituations: async (req, res) => {
    const { page = 1 } = req.query
    const situations = await Situation.paginate({}, { page, limit: 10 })
    return res.json(situations)
  },
  createSituation: async (req, res) => {
    try {
      const situation = await Situation.create(req.body)
      return res.json(situation)
    } catch (err) {    
      return res.status(400).json(err)
    }
  },
  updateSituation: async (req, res) => {
    try {
      const situation = await Situation.findByIdAndUpdate(req.params.id, req.body, { new: true })
      return res.json(situation)
    } catch (err) {    
      return res.status(400).json(err)
    }
  }
}