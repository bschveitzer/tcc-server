const mongoose = require('mongoose')
const User = mongoose.model('User')
const jwt = require('jsonwebtoken')
const error = require('../utils/errors')
const emailService = require('../utils/emailService')
const moment = require('moment')


module.exports = {
  login: async (req, res) => {
    try {
      const userInfo = await User.findOne(req.body, '-password -createdAt -updatedAt').populate('tasks')

      if(!userInfo) return res.status(400).json(error.loginErrors['NotFound'])

      jwt.sign({ userInfo }, 'scoutKey', (err, token) => {
        res.json({ userInfo, token })
      })

    } catch (err) {
      return res.status(400).json(err)
    }
  },

  createUser: async (req, res) => {
    try {
      req.body.password = Math.floor(1000 + Math.random() * 9000)
      const user = await User.create(req.body) 
      return res.json(user)
    } catch (err) {
      const responseErr = error.crudUserErrors[err.name] ? error.crudUserErrors[err.name] : err
      return res.status(400).json(responseErr)
    } 
  },

  recoverPassword: async (req, res) => {
    try {
      const user = await User.findOne(req.body, 'login')
      const timestamp = moment().unix()

      if(!user) return res.status(400).json(error.recoverPasswordErrors['NotFound'])

      const link = `${process.env.FRONT_URL}/resetPassword/${user._id}%${timestamp}`

      let info = await emailService.transporter.sendMail({
        from: '"Não responda" <foo@example.com>', // Endereço do rementente
        to: user.login, // Lista de recebedores
        subject: "Recuperação de senha", // Linha do assunto
        text: "Hello world?", // Conteudo sem HTML
        html: `<a href=${link}>Clique aqui</a>` // Conteudo com HTML
      });

      if(info.accepted.length === 0) return res.status(400).json(error.recoverPasswordErrors['SendingError'])
      
      await User.findByIdAndUpdate(user._id, { recoverPassword: timestamp })

      return res.json({ message: 'Solicitação de recuperação de senha enviada. Verifique seu e-mail.'})
    } catch (err) {
      return res.status(400).json(err)
    }
  },

  resetPassword: async (req, res) => {
    try {
      const { timestamp, password } = req.body
      const user = await User.findById(req.params.id, 'recoverPassword')
      if(!user) return res.status(400).json(error.recoverPasswordErrors['InvalidId'])

      const timestampDiff = moment(timestamp).diff(user.recoverPassword, 'hours')
      if(timestampDiff > 23 || !user.recoverPassword) return res.status(400).json(error.recoverPasswordErrors['ExpiredSolicitation'])

      await User.findByIdAndUpdate(req.params.id, { recoverPassword: '', password })
      
      return res.json({ message: 'Senha atualizada com sucesso.'})
    } catch (err) {
      return res.status(400).json(err)
    }
  }
}