const mongoose = require('mongoose')
const Task = mongoose.model('Task')
const User = mongoose.model('User')
const Project = mongoose.model('Project')
const Interval = mongoose.model('Interval')
const Sprint = mongoose.model('Sprint')
const error = require('../utils/errors')

module.exports = {
  getTasksFromProject: async (req, res) => {
    try {
      const { page = 1 } = req.query
      const tasks = await Task.paginate({ project: req.params.id }, { page, limit: 10 })
      return res.json(tasks)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  createTask: async (req, res) => {
    try {
      const preparedTask = await module.exports.prepareTaskToCreate(req.body.task, req.body.project);
      const task = await Task.create(preparedTask)

      if(task.user) {
        await User.findByIdAndUpdate(task.user, { $addToSet: { 'tasks': task._id } })
      }

      if(task.sprint) {
        await Sprint.findByIdAndUpdate(task.sprint, { $addToSet: { 'tasks': task._id } })
      }

      await Project.findByIdAndUpdate(req.body.project, { $addToSet: { 'tasks': task._id } })

      return res.json(task)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  prepareTaskToCreate: async (task, project) => {
    try {
      const projectObj = await Project.findById(project)

      task.number = projectObj.tasks.length ? projectObj.tasks.length + 1 : 1;
      task.status = 'open';

      return task;
    } catch (err) {
      return err
    }
  },
  updateTask: async (req, res) => {
    try {
      const task = await Task.findByIdAndUpdate(req.params.taskId, req.body, { new: true })
      return res.json(task)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  removeTask: async (req, res) => {
    try {
      const task = await Task.findByIdAndRemove(req.params.id);
      if(task.parentTask) {
        await Task.findByIdAndUpdate(task.parentTask, { $pull: { tasks: req.params.id } })
      }
      await Interval.findOneAndUpdate({ task: req.params.id }, { task: '' })
      return res.json(task)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  getTaskById: async (req, res) => {
    try {
      const task = await Task.findById(req.params.taskId).populate('user').populate('subtasks').populate('parentTask').populate('sprint')
      const project = await Project.findOne({ tasks: req.params.taskId }).populate('sprints')

      return res.json({task, project})
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  addUserToTask: async (req, res) => {
    try {
      const task = await Task.findByIdAndUpdate(req.params.taskId, { user: req.body.user }, { new: true }).populate('user')
      await User.findByIdAndUpdate(req.body.user, { $addToSet: { tasks: req.params.taskId } })
      return res.json(task)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  removeUserFromTask: async (req, res) => {
    try {
      const task = await Task.findByIdAndUpdate(req.params.taskId, { user: null })
      await User.findByIdAndUpdate(req.body.user, { $pull: { tasks: req.params.taskId } })
      return res.json(task)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  setTaskToInterval: async (req, res) => {
    try {
      const interval = await Interval.findByIdAndUpdate(req.body.intervalId, { $pull: { task: req.params.id } })
      return res.json(interval)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  removeTaskFromInterval: async (req, res) => {
    try {
      const interval = await Interval.findByIdAndUpdate(req.body.intervalId, { $pull: { task: '' } })
      return res.json(interval)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  createSubtask: async (req, res) => {
    try {
      const subtask = await Task.create(req.body)
      await Task.findByIdAndUpdate(subtask.parentTask, { $push: { subtasks: subtask.id } })
      return res.json(subtask)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  updateTaskSprint: async (req, res) => {
    try {   
      await Sprint.findOneAndUpdate({tasks: req.params.taskId},  { $pull: { 'tasks': req.params.taskId } })

      let newTask;

      if(req.body.sprint === 'no-sprint') {
        newTask = await Task.findByIdAndUpdate(req.params.taskId, { sprint: null }, { new: true })
      } else {
        newTask = await Task.findByIdAndUpdate(req.params.taskId, { sprint: req.body.sprint }, { new: true })
        await Sprint.findByIdAndUpdate(req.body.sprint ,  { $addToSet: { 'tasks': req.params.taskId } }) 
      }

      return res.json(newTask)
    } catch (err) {
      return res.status(400).json(err)
    }
  },
  async getTaskByName(req, res) {
    try {
      const tasks = await Task.find({'name': {'$regex': req.params.name, '$options': 'i'}})
      return res.json(tasks)
    } catch (err) {    
      return res.status(400).json(err)
    }
  }
}