const mongoose = require('mongoose')
const Sprint = mongoose.model('Sprint')
const error = require('../utils/errors')

module.exports = {
  getSprintsFromProject: async (req, res) => {
    const { page = 1 } = req.query
    const sprints = await Sprint.paginate({ project: req.params.id }, { page, limit: 10 })
    return res.json(sprints)
  },
  getActiveSprint: async (req, res) => {
    const { page = 1 } = req.query
    const sprints = await Sprint.paginate({ project: req.params.id, active: true }, { page, limit: 10 })
    return res.json(sprints)
  },
  createSprint: async (req, res) => {
    try {
      const sprint = await Sprint.create(req.body)
      return res.json(sprint)
    } catch (err) {    
      return res.status(400).json(err)
    }
  },
  updateSprint: async (req, res) => {
    try {
      const sprint = await Sprint.findByIdAndUpdate(req.params.id, req.body, { new: true })
      return res.json(sprint)
    } catch (err) {    
      return res.status(400).json(err)
    }
  }
}