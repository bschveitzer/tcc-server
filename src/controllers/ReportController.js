const mongoose = require('mongoose')
const UserMonthReport = mongoose.model('UserMonthReport')
const UserDayReport = mongoose.model('UserDayReport')
const Interval = mongoose.model('Interval')
const Task = mongoose.model('Task')
const error = require('../utils/errors')
const moment = require('moment')

module.exports = {
  updateUserHoursReport: async (req, res) => { 
    const { user, days } = req.body

    try {
      await days.map((dayIndex) => {
        dayIndex.intervals.map(async (interval) => {
          const objInterval = { ...interval, user }
          delete objInterval.id
  
          if(interval.id) {
            const updateInterval = await Interval.findByIdAndUpdate(
              interval.id,
              objInterval
            )  
          } else {
            const createInterval = await Interval.create(objInterval)
            const dayReport = await UserDayReport.update(
              { month: req.params.monthId, dayOrder: dayIndex.day},
              { $push: { intervals: createInterval._id}},
              { upsert: true }
            )
  
            if(dayReport.upserted && dayReport.upserted.length > 0) {
              await UserMonthReport.findByIdAndUpdate(
                req.params.monthId,
                { $push: { dailyReports: dayReport.upserted[0]._id } }
              )
            }
  
          }
        })
      })

      res.json({ message: 'Horários atualizados com sucesso!'})

    } catch(err) {
      res.status(400).json(err)
    }
  },

  verifyMonthReport: async (req, res) => {
    const { user } = req.params
    try {
      const monthReport = await UserMonthReport.findOne({
        monthOrder: moment().month(),
        year: moment().year(),
        user: user
      })
      
      if(!monthReport) {
        const createMonth = {
          name: moment().month(moment().month()).format('MMMM'),
          monthOrder: moment().month(),
          dailyReports: [],
          year: moment().year(),
          user
        }
        const newMonthReport = await UserMonthReport.create(createMonth) 
        res.json(newMonthReport)
      } else {
        res.json(monthReport)
      }
    } catch (err) {   
      res.status(400).json(err)
    }
  },

  getMonthReport: async (req, res) => {
    try {
      const monthReport = await UserMonthReport.findOne({user: req.params.userId, monthOrder: parseInt(req.params.month), year: parseInt(req.params.year) })
        .populate({ 
          path: 'intervals',
          populate: { 
            path: 'task'
          }
        })
      res.json(monthReport)
    } catch (err) {
      res.status(400).json(err)
    }
    
  },

  getUserReports: async (req, res) => { 
    const { page = 1 } = req.query
    try {
      const userReports = await UserMonthReport.paginate(req.params, { page, limit: 10 })
      res.json(userReports)
    } catch (err) {
      res.status(400).json(err)
    }
  },

  updateIntervals: async (req, res) => { 
    try {
      const intervals = req.body.intervals;
      const monthOrderNum = moment(req.body.originalDate).month();
      const yearNum = moment(req.body.originalDate).year();

      for(let index = 0; index < intervals.length; index++) {
        let updateObj = {...intervals[index]};

        updateObj.dayOrder = req.body.dayOrder;

        if(intervals[index].task) {
          updateObj.task = intervals[index].task.value;
        }


        if(updateObj._id) {
          await Interval.findByIdAndUpdate(updateObj._id, updateObj, { new: true });
        } else {     
          const newInterval = await Interval.create(updateObj);

          const updatedReport = await UserMonthReport.findOneAndUpdate(
            {user: req.body.user._id, monthOrder: monthOrderNum, year: yearNum}, 
            { $addToSet: { 'intervals': newInterval._id } }
          )

          const monthlyReportObj = {
            user: req.body.user._id, 
            monthOrder: monthOrderNum, 
            year: yearNum,
            intervals: [newInterval._id]
          }
          
          if(!updatedReport) {
            await UserMonthReport.create(monthlyReportObj);
          }
        }
      }
      
      const monthlyReport = await UserMonthReport.findOne({user: req.body.user._id, monthOrder: monthOrderNum, year: yearNum})
        .populate({ 
          path: 'intervals',
          populate: { 
            path: 'task'
          }
        })
      res.json(monthlyReport)
    } catch (err) {
      res.status(400).json(err)
    }
  },
}