const mongoose = require('mongoose')
const User = mongoose.model('User')
const error = require('../utils/errors')

module.exports = {
  async getAllUsers(req, res) {
    const { page = 1 } = req.query
    const users = await User.paginate({}, { page, limit: 10 })
    return res.json(users)
  },
  async getUserInfo(req, res) {
    const user = await User.findById(req.params.id).populate('tasks')

    return res.json(user)
  },
  async updateUser(req, res) {
    try {
      const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true })
      return res.json(user)
    } catch (err) {    
      const responseErr = error.crudUserErrors[err.name] ? error.crudUserErrors[err.name] : err
      return res.status(400).json(responseErr)
    }
  },
  async getUserByName(req, res) {
    try {
      const users = await User.find({ $or: [{'name.first': {'$regex': req.params.name, '$options': 'i'}}, {'name.last':{'$regex': req.params.name, '$options': 'i'}}]})
      return res.json(users)
    } catch (err) {    
      const responseErr = error.crudUserErrors[err.name] ? error.crudUserErrors[err.name] : err
      return res.status(400).json(responseErr)
    }
  }
}