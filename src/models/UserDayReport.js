const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const UserDayReportSchema = new mongoose.Schema({
	dayOrder: {
    type: String,
    enum: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
    required: true
  },
	intervals: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Interval'
  }],
  dayHours: {
    type: Number,
    default: 0
  },
  month: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserMonthReport'
  }
}, { timestamps: true, strict: false })

UserDayReportSchema.plugin(mongoosePaginate)

mongoose.model('UserDayReport', UserDayReportSchema)