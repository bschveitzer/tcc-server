const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const UserSchema = new mongoose.Schema({
	login: {
		type: String,
		trim: true,
		required: true,
		unique: true
	},
	password: {
		type: String,
		trim: true,
		required: true
	},
	name: {
		first: {
			type: String,
			trim: true,
			required: true
		},
		last: {
			type: String,
			trim: true,
			required: true
		}
	},
	cpf: {
		type: String,
		required: true,
		unique: true,
		match: /^[0-9]*$/,
		minlength: 11,
		maxlength: 11
	},
	birthdate: {
		type: Date,
		required: true
	},
	reports: [{
		type: mongoose.Schema.Types.ObjectId, 
		ref: 'UserMonthReport'
	}],
	tasks: [{
		type: mongoose.Schema.Types.ObjectId, 
		ref: 'Task'
	}],
	recoverPassword: {
		type: Number
	},
	organization: {
		type: mongoose.Schema.Types.ObjectId, 
		ref: 'Organization'
	}
}, { toJSON: { virtuals: true }, timestamps: true })

UserSchema.plugin(mongoosePaginate)

UserSchema.virtual('fullName').get(function () {
  return `${this.name.first} ${this.name.last}`;
});

mongoose.model('User', UserSchema)