const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const OrganizationSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String
	},
  users: [{
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User' 
  }],
  active: {
    type: Boolean,
    default: true
  }
}, { timestamps: true })

OrganizationSchema.plugin(mongoosePaginate)

mongoose.model('Organization', OrganizationSchema)