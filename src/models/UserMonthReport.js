const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const UserMonthReportSchema = new mongoose.Schema({
	monthOrder: {
    type: Number,
    enum: ['0','1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'],
    required: true
	},
	intervals: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Interval'
  }],
  monthHours: {
    type: Number,
    default: 0
  },
  year: {
    type: Number,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User'
  }
}, { timestamps: true, strict: false })

UserMonthReportSchema.plugin(mongoosePaginate)

mongoose.model('UserMonthReport', UserMonthReportSchema)