const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const SituationSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  value: {
    type: String
  },
  organization: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Organization'
  },
  project: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Project'
  },
  active: {
    type: Boolean,
    default: true
  }
}, { timestamps: true })

SituationSchema.plugin(mongoosePaginate)

mongoose.model('Situation', SituationSchema)