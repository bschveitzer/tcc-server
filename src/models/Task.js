const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const TaskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
  number: {
    type: Number
  },
	description: {
		type: String
	},
  comments: [{
    type: String
  }],
	user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User' 
  },
	subtasks: [{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Task' 
  }],
  estimated: {
    type: Number,
  },
  completed: {
    type: Number
  },
  remain: {
    type: Number
  },
  status: {
    type: String,
    enum: ['open', 'doing', 'committed', 'testing', 'done']
  },
  type: {
    type: String,
    enum: ['backlog', 'bug', 'refactor']
  },
  acceptanceCriteria: [{
    order: {
      type: Number
    },
    description: {
      type: String
    },
    checked: {
      type: Boolean
    },
    accepted: {
      type: Boolean
    }
  }],
  parentTask: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Task'
  },
  sprint: {  
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Sprint'
  }
}, { timestamps: true })

TaskSchema.plugin(mongoosePaginate)

mongoose.model('Task', TaskSchema)