const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const IntervalSchema = new mongoose.Schema({
  clockIn: {
    type: Date,
    required: true
  },
  clockOut: {
    type: Date
  },
  task: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Task'
  },
  dayOrder: {
    type: Number,
    required: true
  }
}, { timestamps: true, strict: false })

IntervalSchema.plugin(mongoosePaginate)

mongoose.model('Interval', IntervalSchema)