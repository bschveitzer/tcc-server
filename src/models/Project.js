const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const ProjectSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String
	},
  organization: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Organization' 
  },
	users: [{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User' 
  }],
	sprints: [{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Sprint' 
  }],
  tasks: [{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Task' 
  }],
  estimated: {
    type: Number,
  },
  completed: {
    type: Number
  },
  remain: {
    type: Number
  }
}, { timestamps: true })

ProjectSchema.plugin(mongoosePaginate)

mongoose.model('Project', ProjectSchema)