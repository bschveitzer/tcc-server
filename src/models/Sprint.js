const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const SprintSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
  number: {
    type: Number,
		required: true
  },
  project: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Project' 
  },
  tasks: [{
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Task' 
  }],
  expectDate: {
    type: Date
  },
  active: {
    type: Boolean
  }
}, { timestamps: true })

SprintSchema.plugin(mongoosePaginate)

mongoose.model('Sprint', SprintSchema)