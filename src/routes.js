const express = require('express')
const routes = express.Router()

const UserController = require('./controllers/UserController')
const OpenController = require('./controllers/OpenController')
const AuthController = require('./controllers/AuthController')
const ReportController = require('./controllers/ReportController')
const SituationController = require('./controllers/SituationController')
const TaskController = require('./controllers/TaskController')
const SprintController = require('./controllers/SprintController')
const ProjectController = require('./controllers/ProjectController')

//Rotas sem autenticação
routes.post('/login', OpenController.login)
routes.post('/user', OpenController.createUser)
routes.post('/recoverPassword', OpenController.recoverPassword)
routes.put('/resetPassword/:id', OpenController.resetPassword)

//Rotas com autenticação
routes.get('/users', AuthController.verifyAuth, UserController.getAllUsers)
routes.get('/user/me', AuthController.verifyAuth)
routes.put('/user/:id', AuthController.verifyAuth, UserController.updateUser)
routes.get('/users/:name', AuthController.verifyAuth, UserController.getUserByName)
routes.get('/user-by-id/:id', AuthController.verifyAuth, UserController.getUserInfo)

routes.get('/report/:userId&:month&:year', AuthController.verifyAuth, ReportController.getMonthReport)
routes.put('/report/:monthId', AuthController.verifyAuth, ReportController.updateUserHoursReport)
routes.get('/reports/:user', AuthController.verifyAuth, ReportController.getUserReports)

routes.get('/situations', AuthController.verifyAuth, SituationController.getAllSituations)


routes.post('/intervals', AuthController.verifyAuth, ReportController.updateIntervals)

routes.get('/task/:taskId', AuthController.verifyAuth, TaskController.getTaskById)
routes.get('/tasks/:projectId', AuthController.verifyAuth, TaskController.getTasksFromProject)
routes.post('/task', AuthController.verifyAuth, TaskController.createTask)
routes.put('/task/:taskId', AuthController.verifyAuth, TaskController.updateTask)
routes.put('/removeTask/:taskId', AuthController.verifyAuth, TaskController.removeTask)
routes.put('/add-user-task/:taskId', AuthController.verifyAuth, TaskController.addUserToTask)
routes.put('/remove-user-task/:taskId', AuthController.verifyAuth, TaskController.removeUserFromTask)
routes.put('/setTaskInterval/:taskId', AuthController.verifyAuth, TaskController.setTaskToInterval)
routes.put('/removeTaskInterval/:taskId', AuthController.verifyAuth, TaskController.removeTaskFromInterval)
routes.post('/subtask', AuthController.verifyAuth, TaskController.createSubtask)
routes.put('/update-sprint-task/:taskId', AuthController.verifyAuth, TaskController.updateTaskSprint)
routes.get('/tasks-by-name/:name', AuthController.verifyAuth, TaskController.getTaskByName)

routes.get('/sprints/:projectId', AuthController.verifyAuth, SprintController.getSprintsFromProject)
routes.get('/sprint/:projectId', AuthController.verifyAuth, SprintController.getActiveSprint)
routes.post('/sprint', AuthController.verifyAuth, SprintController.createSprint)
routes.put('/sprint/:sprintId', AuthController.verifyAuth, SprintController.updateSprint)

routes.get('/projects/:organizationId', AuthController.verifyAuth, ProjectController.getProjectsFromOrg)
routes.get('/project/:projectId', AuthController.verifyAuth, ProjectController.getProjectDetails)
routes.post('/project', AuthController.verifyAuth, ProjectController.createProject)
routes.put('/project/:projectId', AuthController.verifyAuth, ProjectController.updateProject)
routes.put('/add-user-project/:projectId', AuthController.verifyAuth, ProjectController.addUserProject)
routes.put('/remove-user-project/:projectId', AuthController.verifyAuth, ProjectController.removeUserProject)
routes.post('/create-new-sprint/:projectId', AuthController.verifyAuth, ProjectController.createNewSprint)
routes.get('/filter-task-by-sprint/:projectId&:sprintId', AuthController.verifyAuth, ProjectController.filterTasksBySprint)



module.exports = routes